'use server'

import Image from 'next/image'
import Header from './components/onboadingcomp/header'
import Footer from './components/onboadingcomp/footer'
import Banner from './components/onboadingcomp/banner'
import Doctorsays from './components/onboadingcomp/doctorsays'
import Imagegallery from './components/onboadingcomp/imagegallery'
import Newsletter from './components/onboadingcomp/newsletter'
import Ourconcept from './components/onboadingcomp/ourconcept'
import Ourservices from './components/onboadingcomp/ourservices'
import Ourcourse from './components/onboadingcomp/ourcourse'
import Ourvalues from './components/onboadingcomp/ourvalues'
import Whatmakes from './components/onboadingcomp/whatmakes'
import axios from 'axios'
import './globals.css'

const API_URL = "http://localhost:5000";

 const API_PATHS = {
  GET_ALL_CATEGORIES: `${API_URL}/get-all-categories`,
  GET_CONTACTED_LIST: `${API_URL}/contact-list`,
  GET_ALL_PENS: `${API_URL}/get-all-pens`,
  GET_PEN_SUB_CATEGORIES: `${API_URL}/get-pen-subcategory`,
  ADD_PRODUCT: `${API_URL}/Our-services`,
  ADD_HEROIMG: `${API_URL}/hero-image`,
  ADD_TEAM: `${API_URL}/team`,
  ADD_IMG: `${API_URL}/image-gallary`,
  ADD_SERVICES: `${API_URL}/our-services`,
  ADD_GALIMG: `${API_URL}/image-gallary`,
  GET_PRODUCT_BY_ID: `${API_URL}/get-product-by-id`,
};


 const getHeroImage = async() => {
  const response = await axios.get(API_PATHS.ADD_HEROIMG);
  const data = response.data;
  return data
}

const getourservices = async() => {
  const response = await axios.get(API_PATHS.ADD_SERVICES);
  const data = response.data;
  return data
}

const getGalleryimage = async() => {
  const response = await axios.get(API_PATHS.ADD_GALIMG);
  const data = response.data;
  return data
}



export default async function Home() {

  const heroImageData = await getHeroImage()
  const ourServicesdata =await getourservices()
  const galleryImageData = await getGalleryimage()
  return (
   <>
     <header>
      <Header></Header>
     </header>
     <main>
         <Banner heroImageData={heroImageData}></Banner>
         <Ourconcept></Ourconcept>
         <Ourservices ourServicesdata={ourServicesdata}></Ourservices>
         <Whatmakes></Whatmakes>
         <Doctorsays></Doctorsays>
         <Imagegallery galleryImageData={galleryImageData}></Imagegallery>
         <Newsletter></Newsletter>
        </main>
        
     <footer>
      <Footer></Footer>
     </footer>
   </>
  )
}
