'use server'
import axios from "axios";
import Aboutheader from "../components/aboutcomp/aboutheader";
import AboutOurvalues from "../components/aboutcomp/aboutourvalues";
import Aboutusappointment from "../components/aboutcomp/aboutusappointment";
import Ustheteam from "../components/aboutcomp/ustheteam";
import Footer from "../components/onboadingcomp/footer";
const API_URL = "http://localhost:5000";
const API_PATHS = {
    GET_ALL_CATEGORIES: `${API_URL}/get-all-categories`,
    GET_CONTACTED_LIST: `${API_URL}/contact-list`,
    GET_ALL_PENS: `${API_URL}/get-all-pens`,
    GET_PEN_SUB_CATEGORIES: `${API_URL}/get-pen-subcategory`,
    ADD_PRODUCT: `${API_URL}/Our-services`,
    ADD_HEROIMG: `${API_URL}/hero-image`,
    ADD_TEAM: `${API_URL}/team`,
    ADD_IMG: `${API_URL}/image-gallary`,
    ADD_SERVICES: `${API_URL}/our-services`,
    ADD_GALIMG: `${API_URL}/image-gallary`,
    GET_PRODUCT_BY_ID: `${API_URL}/get-product-by-id`,
  };

  const getteam = async() => {
    const response = await axios.get(API_PATHS.ADD_TEAM);
    const data = response.data;
    return data
   
  }

  

const about = async () => {
    const TeamData =  await getteam()
    return ( 
        <>
          <header>
            <Aboutheader></Aboutheader>
            </header>
            <main>
                <AboutOurvalues></AboutOurvalues>
                <Ustheteam TeamData={TeamData}></Ustheteam>
                <Aboutusappointment></Aboutusappointment>
            </main>
            <footer>
                  <Footer></Footer>
            </footer>
        </>
     );
}
 
export default about;