import Script from "next/script";
import { BASE_ASSET_URL } from '../../utils';

const Banner = (props: { heroImageData: any; }) => {
  const { heroImageData } = props;

  // Check if data is present
  if (heroImageData && heroImageData.length > 0) {
    const { heading, subheading, image } = heroImageData[0];

    return (
      <div className="slider_wrap">
        <img src={`${BASE_ASSET_URL}/${image.filename}`} alt="" className="img-responsive" />
        <div className="text_outer">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <p className="wow fadeInUp">{subheading}</p>
                <h2 className="wow fadeInUp">{heading}</h2>
                <a href="https://form.jotform.com/233101901894048" className="btn btn_new1" target="_blank">
                  Inquire Now
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    // Render default content when no data is present
    return (
      <div className="slider_wrap">
        <img src="images/slide1.jpg" alt="" className="img-responsive" />
        <div className="text_outer">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <h2 className="wow fadeInUp">
                  Torem ipsum dolor sit amet, consectetur adipiscing elit.
                </h2>
                <p className="wow fadeInUp">
                  Horem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et velit interdum, ac aliquet odio mattis.
                </p>
                <a href="#" className="btn btn_new1">
                  Inquire Now
                </a>
                <a href="#" className="btn btn_new2">
                  Book Our Courses
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Banner;
