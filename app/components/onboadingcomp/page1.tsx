const Page1 = () => {
    return ( 
        <>
        <head>
        <meta charSet="utf-8" />
  <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
  <meta
    name="viewport"
    content="width=device-width, initial-scale=1,maximum-scale=1.0,user-scalable=0"
  />
  <title>Cat Care</title>
  <link href="css/bootstrap.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="css/animate.css" type="text/css" />
  <link href="css/owl.carousel.css" rel="stylesheet" />
  <link href="css/owl.theme.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" />
  <link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css"
  />
  {/* Font Icons CSS */}
  <link rel="stylesheet" href="css/ionicons.css" />
  {/* Droopmenu CSS */}
  <link rel="stylesheet" href="css/droopmenu.css" />
  <link rel="stylesheet" href="css/hr-timePicker.min.css" />
  <link
    rel="stylesheet"
    href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
  />
  <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="" />
  <link
    href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@500&display=swap"
    rel="stylesheet"
  />
  {/* HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries */}
  {/* WARNING: Respond.js doesn't work if you view the page via file:// */}
  {/*[if lt IE 9]>


    <![endif]*/}
        </head>
  <div className="container">
    <div className="row">
      <div className="col-md-8 col-md-offset-2">
        <div className="booking_1">
          <div className="row">
            <div className="col-md-6">
              <h2>CatCare</h2>
              <p>
                Shop No.1F Block Prime Colony <a href="#">View Details</a>{" "}
              </p>
            </div>
            <div className="col-md-6 cancel_right">
              {" "}
              <a href="#" className="btn btn_new1">
                Cancel Request
              </a>{" "}
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
        <div className="clearfix" />
        <div className="booking_2">
          <div className="booking_2_a">
            <h2>
              <i className="las la-check-circle" /> Booking Confirmed
            </h2>{" "}
          </div>
          <div className="clearfix" />
          <div className="booking_2_b">
            <div className="doc_left">
              {" "}
              <img
                src="images/doctor.png"
                className="img-responsive"
                alt=""
              />{" "}
            </div>
            <div className="doc_right">
              <h5>
                Doctor will be assigned <span>1 hour</span> before the scheduled
                time
              </h5>
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
          <div className="booking_2_c">
            <div className="row">
              <div className="col-md-6">
                <h6>Dr William Hemsflow</h6>
                <h5>10-11 AM on friday, 23rd November 2023</h5>{" "}
              </div>
              <div className="col-md-6 re_chedule">
                <button className="btn btn_new2">Re-schedule</button>
              </div>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
      </div>
      <div className="clearfix" />
    </div>
    <div className="clearfix" />
  </div>
  <div id="myModal" className="modal fade new_modal" role="dialog">
    <div className="modal-dialog">
      {/* Modal content*/}
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal">
            ×
          </button>
          <h4 className="modal-title">Enter OTP</h4>{" "}
        </div>
        <div className="modal-body">
          <div className="row">
            <div className="col-md-6">
              <img
                src="images/otp.jpg"
                className="img-responsive center-block"
                alt=""
              />{" "}
            </div>
            <div className="col-md-6">
              <h3>Please Enter Verification Code</h3>
              <div className="veri_box">
                <div className="form-group">
                  <input type="email" className="form-control" id="email" />{" "}
                </div>
              </div>
              <div className="veri_box">
                <div className="form-group">
                  <input type="email" className="form-control" id="email" />{" "}
                </div>
              </div>
              <div className="veri_box">
                <div className="form-group">
                  <input type="email" className="form-control" id="email" />{" "}
                </div>
              </div>
              <div className="veri_box">
                <div className="form-group">
                  <input type="email" className="form-control" id="email" />{" "}
                </div>
              </div>
              <div className="clearfix" />
              <button className="btn btn_new2">Submit</button>
            </div>
            <div className="clearfix" />
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn_new1" data-dismiss="modal">
            Close
          </button>
        </div>
      </div>
    </div>
  </div>
</>

     );
}
 
export default Page1;