"use client"
import Script from "next/script";

const Ourcourse = () => {
    return ( 
        <>
        <div className="service_green">
  <div className="container concept_wrap">
    <div className="row">
      <div className="col-md-9 col-sm-9 concept_top">
        <h3>Our Courses</h3>
        <p>Gorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>
      <div className="col-md-3 col-sm-3">
        <div className="form-group">
          <select className="form-control" id="sel1">
            <option>Select Location</option>
            <option>Sydney</option>
            <option>Perth</option>
            <option>Adelaide</option>
          </select>
        </div>
      </div>
      <div className="clearfix" />
      <div className="col-md-4">
        <div className="service_box" data-uniform="true2">
          <div className="sb_padding">
            {" "}
            <img src="images/i7.png" alt="" />
            <h4>Specialized Courses</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip exea.{" "}
            </p>
            <div className="row">
              <div className="col-xs-6">
                {" "}
                <a href="#">Read More</a>{" "}
              </div>
              <div className="col-xs-6 book_button">
                {" "}
                <a href="#" className="btn btn_new1">
                  Book
                </a>{" "}
              </div>
              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </div>
      <div className="col-md-4">
        <div className="service_box" data-uniform="true2">
          <div className="sb_padding">
            {" "}
            <img src="images/i7.png" alt="" />
            <h4>Specialized Courses</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip exea.{" "}
            </p>
            <div className="row">
              <div className="col-xs-6">
                {" "}
                <a href="#">Read More</a>{" "}
              </div>
              <div className="col-xs-6 book_button">
                {" "}
                <a href="#" className="btn btn_new1">
                  Book
                </a>{" "}
              </div>
              <div className="clearfix" />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-4">
        <div className="service_box" data-uniform="true2">
          <div className="sb_padding">
            {" "}
            <img src="images/i7.png" alt="" />
            <h4>Specialized Courses</h4>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip exea.{" "}
            </p>
            <div className="row">
              <div className="col-xs-6">
                {" "}
                <a href="#">Read More</a>{" "}
              </div>
              <div className="col-xs-6 book_button">
                {" "}
                <a href="#" className="btn btn_new1">
                  Book
                </a>{" "}
              </div>
              <div className="clearfix" />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="clearfix" />
  </div>
</div>

<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
	<Script src="js/bootstrap.min.js"></Script>
	{/* <Script type="text/javaScript" src="js/custom.js"></Script> */}
	<Script type="text/javaScript" src="js/droopmenu.js"></Script>
	{/* <Script type="text/javaScript">
		jQuery(function ($) {
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			});
		});
	</Script> */}
	
	<Script src="js/wow.js"></Script>
	<Script>
		new WOW().init();
	</Script>
	<Script src="js/owl.carousel.js"></Script>
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 3], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, dots: true
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</Script> */}

	<Script src="js/waypoints.min.js" type="text/javaScript"></Script>
	<Script src="js/jquery.counterup.min.js" type="text/javaScript"></Script>
	{/* <Script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			});
		});
	</Script> */}
	
	
	{/* <Script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</Script> */}
	
	
	
	
	
	
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</Script> */}
	<Script src="js/jquery.easing.min.js"></Script>
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true2]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true2]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	
	
	
	
	
	
	
	<Script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></Script>
	{/* <Script type="text/javaScript">
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: "dd-mm-yy"
				, duration: "fast"
			});
		});
	</Script> */}
	
        </>
     );
}
 
export default Ourcourse;