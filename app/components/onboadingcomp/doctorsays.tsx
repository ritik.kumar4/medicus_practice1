"use client"

import Script from "next/script";
import React, { useEffect, useState } from 'react';
//@ts-ignore 
import ItemsCarousel from 'react-items-carousel';

const Doctorsays = () => {
  const [activeItemIndex, setActiveItemIndex] = useState(0);
  const [numberOfCards, setNumberOfCards] = useState(3);
  const chevronWidth = 40;
  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth <= 768) {
        setNumberOfCards(1);
      } else {
        setNumberOfCards(4);
      }
    };
    window.addEventListener('resize', handleResize);

    handleResize();

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
    return ( 
        <>
<div className="doctors_say">
    <div className="container concept_wrap" style={{marginTop:-20}}>
      <div className="row">
        <div className="col-md-12 doctors_top">
          <h3>What our doctors say about our Practice</h3>
        </div>
        <div className="clearfix" />
        <div className="col-md-12 diff">
        <div style={{ padding: `0 ${chevronWidth}px` }}>
      <ItemsCarousel
   requestToChangeActive={setActiveItemIndex}
   activeItemIndex={activeItemIndex}
   numberOfCards={numberOfCards}
   gutter={20}
   outsideChevron
   chevronWidth={chevronWidth}
      >
                  <div className="item">
              <div className="diff_box" data-uniform="true3">
                <div className="diff_inner">
                  <div className="diff_left">
                    {" "}
                    <img
                      src="images/doc1.jpg"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </div>
                  <div className="diff_right">
                    <h5>Dr. Ernst Müller</h5>
                    <h6>Dermatology</h6>{" "}
                  </div>
                  <div className="clearfix" />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliquat enim ad minim veniam.
                  </p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="diff_box" data-uniform="true3">
                <div className="diff_inner">
                  <div className="diff_left">
                    {" "}
                    <img
                      src="images/doc2.jpg"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </div>
                  <div className="diff_right">
                    <h5>Dr. Ernst Müller</h5>
                    <h6>Dermatology</h6>{" "}
                  </div>
                  <div className="clearfix" />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliquat enim ad minim veniam.
                  </p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="diff_box" data-uniform="true3">
                <div className="diff_inner">
                  <div className="diff_left">
                    {" "}
                    <img
                      src="images/doc3.jpg"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </div>
                  <div className="diff_right">
                    <h5>Dr. Ernst Müller</h5>
                    <h6>Dermatology</h6>{" "}
                  </div>
                  <div className="clearfix" />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliquat enim ad minim veniam.
                  </p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="diff_box" data-uniform="true3">
                <div className="diff_inner">
                  <div className="diff_left">
                    {" "}
                    <img
                      src="images/doc1.jpg"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </div>
                  <div className="diff_right">
                    <h5>Dr. Ernst Müller</h5>
                    <h6>Dermatology</h6>{" "}
                  </div>
                  <div className="clearfix" />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliquat enim ad minim veniam.
                  </p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="diff_box" data-uniform="true3">
                <div className="diff_inner">
                  <div className="diff_left">
                    {" "}
                    <img
                      src="images/doc2.jpg"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </div>
                  <div className="diff_right">
                    <h5>Dr. Ernst Müller</h5>
                    <h6>Dermatology</h6>{" "}
                  </div>
                  <div className="clearfix" />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliquat enim ad minim veniam.
                  </p>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="diff_box" data-uniform="true3">
                <div className="diff_inner">
                  <div className="diff_left">
                    {" "}
                    <img
                      src="images/doc3.jpg"
                      className="img-responsive"
                      alt=""
                    />{" "}
                  </div>
                  <div className="diff_right">
                    <h5>Dr. Ernst Müller</h5>
                    <h6>Dermatology</h6>{" "}
                  </div>
                  <div className="clearfix" />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliquat enim ad minim veniam.
                  </p>
                </div>
              </div>
            </div>
      </ItemsCarousel>
    </div>
          <div className="customNavigation1">
            <a onClick={()=>setActiveItemIndex((pre)=>pre-1)} className="btn prev1">
              <img src="images/l_o.png" />
            </a>
            <a onClick={()=>setActiveItemIndex((pre)=>pre+1)}  className="btn next1">
              <img src="images/r_o.png" />
            </a>
          </div>
        </div>
        <div className="clearfix" />
      </div>
      <div className="clearfix" />
    </div>
  </div>
  <div className="modal fade advert_modal" id="myModal_login" role="dialog">
    <div className="modal-dialog">
      {/* Modal content*/}
      <div className="modal-content">
        <div className="modal-header">
          <button type="button" className="close" data-dismiss="modal">
            ×
          </button>
        </div>
        <div className="modal-body">
          <div className="login_wrap login_box">
            <ul className="nav nav-pills nav-justified">
              <li className="active">
                <a data-toggle="pill" href="#customer">
                  Login
                </a>
              </li>
              <li>
                <a data-toggle="pill" href="#agency">
                  Register
                </a>
              </li>
            </ul>
            <div className="tab-content">
              <div id="customer" className="tab-pane fade in active">
                <form action="/action_page.php" className="form_div_outer2">
                  <div className="form-group">
                    <label htmlFor="email">Email address:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Phone:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd">Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      id="pwd"
                    />{" "}
                  </div>{" "}
                  <a
                    href="javascript:void(0);"
                    className="forgot"
                    data-toggle="collapse"
                    data-target="#fgot2"
                  >
                    Forgot Password?
                  </a>
                  <div id="fgot2" className="collapse">
                    <h6>
                      We shall send a re-activation link to your email address
                    </h6>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Enter Email Address"
                      />{" "}
                    </div>
                    <button className="btn btn_new1">Submit</button>
                  </div>
                  <div className="checkbox">
                    <label>
                      <input type="checkbox" /> Stay Logged In
                    </label>
                  </div>
                  <div className="login_lower">
                    <button type="submit" className="btn btn_new1">
                      Submit
                    </button>
                  </div>
                </form>
                <div className="clearfix" />
                <div className="reg_link">
                  <p>
                    If you dont have any account then register here -{" "}
                    <a href="#agency" className="link-to-tab">
                      Register
                    </a>
                  </p>
                </div>
              </div>
              <div id="agency" className="tab-pane fade">
                <form action="/action_page.php" className="form_div_outer2">
                  <div className="form-group">
                    <label htmlFor="email">Name:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Email address:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Phone:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="phone"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd">Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      id="pwd"
                    />{" "}
                  </div>
                  <div className="form-group">
                    <label htmlFor="pwd">Confirm Password:</label>
                    <input
                      type="password"
                      className="form-control"
                      id="pwd"
                    />{" "}
                  </div>
                  <div className="login_lower">
                    <button type="submit" className="btn btn_new1">
                      Submit
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn_new1" data-dismiss="modal">
            Close
          </button>
        </div>
      </div>
    </div>
  </div>
  <div className="scroll-top-wrapper">
    {" "}
    <span className="scroll-top-inner">
      <img src="images/return2.png" className="img-responsive" />
    </span>{" "}
  </div>
   
	<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
	<Script src="js/bootstrap.min.js"></Script>
	{/* <Script type="text/javaScript" src="js/custom.js"></Script> */}
	<Script type="text/javaScript" src="js/droopmenu.js"></Script>
	{/* <Script type="text/javaScript">
		jQuery(function ($) {
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			})
		});
	</Script> */}
	
	<Script src="js/wow.js"></Script>
	<Script>
		new WOW().init();
	</Script>
	<Script src="js/owl.carousel.js"></Script>
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			var owl = $("#owl-demo1");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 2], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 2], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, pagination: false
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next1").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev1").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play1").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop1").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</Script> */}

	<Script src="js/waypoints.min.js" type="text/javaScript"></Script>
	<Script src="js/jquery.counterup.min.js" type="text/javaScript"></Script>
	{/* <Script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			})
		});
	</Script> */}
	
	
	{/* <Script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</Script> */}
	
	
	
	
	
	
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</Script> */}
	<Script src="js/jquery.easing.min.js"></Script>
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true2]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true2]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true3]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true3]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<Script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></Script>
	{/* <Script type="text/javaScript">
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: "dd-mm-yy"
				, duration: "fast"
			});
		});
	</Script>
	 */}

        </>
     );
}
 
export default Doctorsays;