"use client"
import Script from "next/script";


const Footer = () => {
    return ( 
        <>
        <div className="footer">
  <div className="container">
    <div className="row">
      <div className="col-md-3 footer_left">
	  <div className="row">
		</div> 
		<div style={{display:"flex", alignItems:"center"}}>
		<img
                      src="images/logo.png"
                      className="img-responsive center-block"
                      alt=""
					  height={40}
					  width={40}
                    />
		<p style={{margin:0}}>©2023 Medicus Practice</p>
		</div>
       
      </div>
      <div className="col-md-9 footer_right">
        <ul>
          <li>
            <a href="#">Impressum </a>
          </li>
          <li>
            <a href="#">Datenschutz</a>
          </li>
          <li>
            <a href="#">Datenschutz Einstellungen</a>
          </li>
          <li>
            <a href="#">AGBs</a>
          </li>
          <li>
            <a href="#">Support &amp; Resources</a>
          </li>
          <li>
            <a href="#">
              {" "}
              <img src="images/arrow_up.png" alt="" />
            </a>
          </li>
        </ul>
      </div>
      <div className="clearfix" />
    </div>
    <div className="clearfix" />
  </div>
</div>

<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
	<Script src="js/bootstrap.min.js"></Script>
	{/* <Script type="text/javaScript" src="js/custom.js"></Script> */}
	<Script type="text/javaScript" src="js/droopmenu.js"></Script>
	{/* <Script type="text/javaScript">
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			});
		});
	</Script> */}
	
	<Script src="js/wow.js"></Script>
	<Script>
		new WOW().init();
	</Script>
	<Script src="js/owl.carousel.js"></Script>
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 3], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, dots: true
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</Script> */}

	<Script src="js/waypoints.min.js" type="text/javaScript"></Script>
	<Script src="js/jquery.counterup.min.js" type="text/javaScript"></Script>
	{/* <Script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			});
		});
	</Script> */}
	
	
	{/* <Script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</Script> */}
	
	
	
	<Script src="js/jquery.fancybox.min.js"></Script>
	{/* <Script type="text/javaScript">
		$("[data-fancybox]").fancybox({
			selector: '[data-fancybox="images"]'
			, loop: true
		});
		$.fancybox.defaults.buttons = [
			  'slideShow'
				, 'zoom'
			  , 'thumbs'
			  , 'fullScreen'
			  , 'close'
			];
	</Script> */}
	
	
	
	
	
	
	
	
{/* 	
	<Script type="text/javaScript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</Script> */}
	<Script src="js/jquery.easing.min.js"></Script>
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true2]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true2]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}
</>
     );
}
 
export default Footer;