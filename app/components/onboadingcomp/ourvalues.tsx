"use client"

import Script from "next/script";

const Ourvalues = () => {
    return ( 
        <>
        <div className="container concept_wrap">
  <div className="row">
    <div className="col-md-12 concept_top">
      <h3>Our Values</h3>
      <p>
        In the Medicus Practice, we are approaching healthcare in new ways: it
        is modern, digital and with the highest level of comfort.With our
        360-degree approach, we combine medical excellence, prevention, and
        therapy specialists in order to be able to offer tailored treatments and
        therapies.Enjoy friendly service and luxurious feelgood moments during
        your treatment stay.
      </p>
    </div>
    <div className="clearfix" />
  </div>
  <div className="row values margin1">
    <div className="col-xs-3">
      <h5>Value 1</h5>
    </div>
    <div className="col-xs-3">
      <h5>Value 2</h5>
    </div>
    <div className="col-xs-3">
      <h5>Value 3</h5>
    </div>
    <div className="col-xs-3">
      <h5>Value 4</h5>
    </div>
    <div className="clearfix" />
  </div>
  <div className="clearfix" />
  <div className="row">
    <div className="col-md-12 values">
      <img
        src="images/beats.png"
        className="img-responsive center-block"
        alt=""
      />
    </div>
    <div className="clearfix" />
  </div>
  <div className="clearfix" />
  <div className="row values">
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="clearfix" />
  </div>
  <div className="clearfix" />
</div>

<Script src="https://code.jquery.com/jquery-3.2.0.min.js"></Script>
	<Script src="js/bootstrap.min.js"></Script>
	{/* <Script type="text/javaScript" src="js/custom.js"></Script> */}
	<Script type="text/javaScript" src="js/droopmenu.js"></Script>
	{/* <Script type="text/javaScript">
		jQuery(function ($) {
			$('.droopmenu-navbar').droopmenu({
				dmArrow: true
				, dmOffCanvas: true
				, dmOffCanvasPos: 'dmoffleft'
				, dmArrowDirection: 'dmarrowup'
			});
		});
	</Script> */}
	
	<Script src="js/wow.js"></Script>
	<Script>
		new WOW().init();
	</Script>
	<Script src="js/owl.carousel.js"></Script>
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			var owl = $("#owl-demo");
			owl.owlCarousel({
				items: 3, //10 items above 1000px browser width
				itemsDesktop: [1000, 3], //5 items between 1000px and 901px
				itemsDesktopSmall: [900, 2], // betweem 900px and 601px
				itemsTablet: [600, 1], //2 items between 600 and 0
				autoPlay: 5000
				, rewindNav: true
				, rewindSpeed: 0
				, dots: true
				, itemsMobile: [479, 1] // itemsMobile disabled - inherit from itemsTablet option
			});
			// Custom Navigation Events
			$(".next").click(function () {
				owl.trigger('owl.next');
			})
			$(".prev").click(function () {
				owl.trigger('owl.prev');
			})
			$(".play").click(function () {
				owl.trigger('owl.play', 1000); //owl.play event accept autoPlay speed as second parameter
			})
			$(".stop").click(function () {
				owl.trigger('owl.stop');
			})
		});
	</Script> */}

	<Script src="js/waypoints.min.js" type="text/javaScript"></Script>
	<Script src="js/jquery.counterup.min.js" type="text/javaScript"></Script>
	{/* <Script>
		jQuery(document).ready(function ($) {
			$('.number_box span').counterUp({
				delay: 10, // the delay time in ms
				time: 1000 // the speed time in ms
			});
		});
	</Script>
	 */}
	
	{/* <Script>
		var a = 0;
		$(window).scroll(function () {
			var oTop = $('#counter').offset().top - window.innerHeight;
			if (a == 0 && $(window).scrollTop() > oTop) {
				$('.counter-value').each(function () {
					var $this = $(this)
						, countTo = $this.attr('data-count');
					$({
						countNum: $this.text()
					}).animate({
						countNum: countTo
					}, {
						duration: 2000
						, easing: 'swing'
						, step: function () {
							$this.text(Math.floor(this.countNum));
						}
						, complete: function () {
							$this.text(this.countNum);
							//alert('finished');
						}
					});
				});
				a = 1;
			}
		});
	</Script> */}
	
	
	
	
	
	
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			$(".mCustomScrollbar").mCustomScrollbar({
				axis: "x"
			});
		});
	</Script> */}
	<Script src="js/jquery.easing.min.js"></Script>
	
	
	
	
	
	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	{/* <Script type="text/javaScript">
		$(document).ready(function () {
			if ($(window).width() > 992) {
				var Uniform = {
					Boot: function () {
						var b = 0;
						$("*[data-uniform=true2]").each(function () {
							if ($(this).height() > b) {
								b = $(this).height();
							}
						});
						$("*[data-uniform=true2]").height(b);
					}
				};
				$(function () {
					Uniform.Boot();
				});
			}
		});
	</Script> */}

	
	
	
	
	
	
	
	
	
	<Script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></Script>
	{/* <Script type="text/javaScript">
		$(function () {
			$("#datepicker").datepicker({
				dateFormat: "dd-mm-yy"
				, duration: "fast"
			});
		});
	</Script> */}

</>


     );
}
 
export default Ourvalues;