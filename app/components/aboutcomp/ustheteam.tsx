"use client";

import { BASE_ASSET_URL } from "@/app/utils";
import Script from "next/script";

const Ustheteam = (props: { TeamData: any }) => {
  console.log(props.TeamData);

  if (props.TeamData.length === 0) {
    return (
      <>
        <div className="the_team">
          <div className="container concept_wrap">
            <div className="row">
              {/* Default team member cards */}
              <div className="col-md-3">
                <div className="concept_box">
                  <div className="concept_top">
                    {" "}
                    <img
                      src="images/doc1.jpg"
                      className="img-responsive center-block"
                      alt=""
                    />{" "}
                  </div>
                  <div className="concept_text" data-uniform="true">
                    <div className="concept_padding">
                      <h5>Name of Founder</h5>
                      <p>CEO</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="concept_box">
                  <div className="concept_top">
                    {" "}
                    <img
                      src="images/doc2.jpg"
                      className="img-responsive center-block"
                      alt=""
                    />{" "}
                  </div>
                  <div className="concept_text" data-uniform="true">
                    <div className="concept_padding">
                      <h5>Name of Founder</h5>
                      <p>CEO</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="concept_box">
                  <div className="concept_top">
                    {" "}
                    <img
                      src="images/doc3.jpg"
                      className="img-responsive center-block"
                      alt=""
                    />{" "}
                  </div>
                  <div className="concept_text" data-uniform="true">
                    <div className="concept_padding">
                      <h5>Name of Founder</h5>
                      <p>CEO</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <div className="concept_box">
                  <div className="concept_top">
                    {" "}
                    <img
                      src="images/doc4.jpg"
                      className="img-responsive center-block"
                      alt=""
                    />{" "}
                  </div>
                  <div className="concept_text" data-uniform="true">
                    <div className="concept_padding">
                      <h5>Name of Founder</h5>
                      <p>CEO</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </>
    );
  }

  // Render team member cards from props.TeamData
  return (
    <>
      <div className="the_team">
        <div className="container concept_wrap">
          <div className="row">
            <div className="col-md-12 concept_top">
              <h3>The Team behind Medicus Practice</h3>
              <p>
                Korem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
                turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec
                fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus
                elit sed risus. Maecenas eget condimentum velit, sit amet feugiat
                lectus.
              </p>
            </div>
            <div className="clearfix" />
            {props.TeamData.map((teamMember: any, index: number) => (
              <div className="col-md-3" key={index}>
                <div className="concept_box">
                  <div className="concept_top">
                    {" "}
                    <img
                      src={`${BASE_ASSET_URL}/${teamMember.image.filename}`}
                      alt=""
                      className="img-responsive"
                    />{" "}
                  </div>
                  <div className="concept_text" data-uniform="true">
                    <div className="concept_padding">
                      <h5>{teamMember.name}</h5>
                      <p>{teamMember.designation}</p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="clearfix" />
        </div>
      </div>

      {/* Add your script tags here */}
    </>
  );
};

export default Ustheteam;
