"use client"

import Script from "next/script";

const AboutOurvalues = () => {
    return ( 
        <>
        <div className="container concept_wrap">
  <div className="row">
    <div className="col-md-12 concept_top">
      <h3>Our Story</h3>
      <p>
        In the Medicus Practice, we are approaching healthcare in new ways: it
        is modern, digital and with the highest level of comfort.With our
        360-degree approach, we combine medical excellence, prevention, and
        therapy specialists in order to be able to offer tailored treatments and
        therapies.Enjoy friendly service and luxurious feelgood moments during
        your treatment stay.
      </p>
    </div>
    <div className="clearfix" />
  </div>
  <div className="row values margin1">
    <div className="col-xs-3">
      <h5>Value 1</h5>
    </div>
    <div className="col-xs-3">
      <h5>Value 2</h5>
    </div>
    <div className="col-xs-3">
      <h5>Value 3</h5>
    </div>
    <div className="col-xs-3">
      <h5>Value 4</h5>
    </div>
    <div className="clearfix" />
  </div>
  <div className="clearfix" />
  <div className="row">
    <div className="col-md-12 values">
      <img
        src="images/beats.png"
        className="img-responsive center-block"
        alt=""
      />
    </div>
    <div className="clearfix" />
  </div>
  <div className="clearfix" />
  <div className="row values">
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="col-xs-3">
      <p>Yorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
    <div className="clearfix" />
  </div>
  <div className="clearfix" />
</div>

</>


     );
}
 
export default AboutOurvalues;